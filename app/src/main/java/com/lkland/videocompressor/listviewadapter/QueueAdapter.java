package com.lkland.videocompressor.listviewadapter;

import java.util.ArrayList;

import com.lkland.videocompressor.R;
import com.lkland.videocompressor.video.IVideo;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class QueueAdapter extends RecyclerView.Adapter<QueueListViewHolder>{

	private ArrayList<IVideo> mList;
	private Activity mActivity;
	private View.OnClickListener mOnClickListener;

	public QueueAdapter(Activity act, ArrayList<IVideo> list){
		this.mList = list;
		mActivity = act;
	}

	public QueueAdapter(Activity act){
		this.mList = new ArrayList<IVideo>();
		mActivity = act;
	}

	public void add(IVideo video){
		mList.add(video);
		this.notifyDataSetChanged();
	}

	public void remove(IVideo video){
		int index = mList.indexOf(video);
		if(index!=-1)
			remove(index);

	}

	public void remove(int index){
		mList.remove(index);
		this.notifyItemRemoved(index);
	}

	public void setOnClickListener(View.OnClickListener lis){
		this.mOnClickListener = lis;
	}

	public void clear(){
		mList.clear();
		this.notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return mList.size();
	}


	@Override
	public void onBindViewHolder(QueueListViewHolder vh, int position) {
		vh.Name.setText(this.mList.get(position).getInName());
		vh.Cancel.setTag(this.mList.get(position));
	}

	@Override
	public QueueListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater =(LayoutInflater)mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.list_item_queue, parent, false);
        QueueListViewHolder vh = new QueueListViewHolder(convertView);
        vh.Cancel.setOnClickListener(mOnClickListener);
        return vh;
	}
}
class QueueListViewHolder extends RecyclerView.ViewHolder{

	public QueueListViewHolder(View itemView) {
		super(itemView);
		Name = (TextView)itemView.findViewById(R.id.tvName);
		Cancel = (ImageView)itemView.findViewById(R.id.IvCancel);

	}
	TextView Name;
	ImageView Cancel;
}